import { splitText, toText } from '@inkylabs/remark-utils'
import { visit } from 'unist-util-visit'
import { visitParents } from 'unist-util-visit-parents'

const NAME = 'easytext'

// TODO: This should be configurable.
// Intentionally excluded as possibly coming at the end of a sentence:
// - Dr.
// - St.
const abbrvRe = new RegExp('(?<=\\b([A-Z]|' + [
  'v',
  'vs',
  'vv',
  'cf',
  'passim'
]
  .map(t => [t.charAt(0), t.slice(1)])
  .map(([t1, t2]) => `[${t1.toUpperCase()}${t1}]${t2}`)
  .join('|') + ')\\.)[ \n]', 'g')

function abbrvNodes (text, position) {
  return splitText(text, abbrvRe, position)
    .map(n => n.match
      ? {
          type: 'controlspace',
          position: n.position
        }
      : {
          type: 'text',
          value: n.text,
          position: n.position
        })
}

export default (opts) => {
  opts = Object.assign({
    ignoretypes: {}
  }, opts)
  opts.ignoretypes = Object.assign({
    'en-dash-range': [],
    'footnote-space': []
  }, opts.ignoretypes)

  return (root, f) => {
    // Make space non-breaking before certain nodes.
    visit(root, ['cite', 'ref'], (node, index, parent) => {
      if (!index) return
      let last
      visit(parent.children[index - 1], 'text', n => { last = n })
      if (!last) return
      last.value = last.value.replace(/ *[\n ]$/, '\u00A0')
    })

    // Find double spaces.
    visit(root, 'image', (node, index, parent) => {
      if (node.alt.includes('  ')) {
        f.message('Text should not contain double space', node.position,
          `${NAME}:no-double-space`)
      }
    })
    visitParents(root, 'text', (node, parents) => {
      const t = (re, msg, rule) => {
        const ig = opts.ignoretypes[rule] || []
        if (parents.some(p => ig.includes(p.type))) return
        splitText(node.value, re, node.position)
          .filter(n => !!n.match)
          .forEach(n => {
            f.message(msg, n.position, `${NAME}:${rule}`)
          })
      }
      t(/ {2}/g, 'Text should not contain double space', 'no-double-space')
      t(/ +\n/g, 'Text should not contain trailing white space', 'no-tws')
      t(/["`']/g, 'Text should not contain plain quote marks',
        'no-plain-quotes')
      t(/[”’][,.;?!]/g, 'Punctuation should not come after quotes',
        'american-punctuation')
      t(/--/g, 'Text should not contain double dashes', 'no-double-dashes')
      t(/([iI]\.e\.|[eE]\.g\.)(?!,)/g,
        'Abbreviation should be followed by a comma', 'abbrv-comma')
      t(/(\d[a-e]?[-—]\d)/g, 'Ranges should use en-dashes', 'en-dash-range')
    })

    // Handle abbreviations ending in a full-stop.
    visit(root, 'image', (node, index, parent) => {
      node.altnode = {
        type: 'span',
        children: abbrvNodes(node.alt, node.position),
        position: node.position
      }
    })
    visit(root, 'text', (node, index, parent) => {
      const newNodes = abbrvNodes(node.value, node.position)
      parent.children.splice(index, 1, ...newNodes)
    })
    visit(root, 'footnote', (node, index, parent) => {
      const nexti = index + 1
      if (nexti >= parent.children.length) return
      const next = parent.children[nexti]
      if (opts.ignoretypes['footnote-space'].includes(next.type)) return
      if (!toText(next, f).match(/^\s/)) {
        f.message('Footnotes should be followed by a space', node.position,
          `${NAME}:footnote-space`)
      }
    })

    const tmap = {
      d: 'controlspace',
      frak: 'fraktur',
      hang: 'hang',
      n: 'linebreak',
      sc: 'smallcaps',
      shy: 'softhyphen',
      sub: 'subscript',
      sup: 'superscript',
      t: 'medspace',
      noindent: 'noindent',
      u: 'underline'
    }
    const types = Object.keys(tmap).map(t => `text-${t}`)
    visit(root, types, (node, index, parent) => {
      node.type = tmap[node.type.replace(/^text-/, '')]
      const next = parent.children[index + 1]
      if (!next || next.type !== 'text') return
      if (node.type === 'controlspace') {
        next.value = next.value.replace(/^ +/, '')
      }
    })
  }
}
